package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import RegisterBean.RegisterBean;
import util.DBConnection;

public class RegisterDao {

	public String registerUser(RegisterBean registerBean) {
		String Enterusername = registerBean.getEnterusername();
		String Email = registerBean.getEmail();
		String password = registerBean.getpassword();
		String cities = registerBean.getcities();
		String Qualification = registerBean.getQualification();

		Connection con = null;
		PreparedStatement preparedStatement = null;

		try {
			con = DBConnection.createConnection();
			String query = "insert into jobseekerregistration(SlNo,Enterusername,Email,password,cities,Qualification) values (NULL,?,?,?,?,?)"; // Insert
																																// user
																																// details
																																// into
																																// the
																																// table
																																// 'USERS'
			preparedStatement = con.prepareStatement(query); // Making use of prepared statements here to insert bunch
																// of data
			preparedStatement.setString(1, Enterusername);
			preparedStatement.setString(2, Email);
			preparedStatement.setString(3, password);
			preparedStatement.setString(4, cities);
			preparedStatement.setString(5, Qualification);

			int i = preparedStatement.executeUpdate();

			if (i != 0) // Just to ensure data has been inserted into the database
				return "SUCCESS";
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return "Oops.. Something went wrong there..!"; // On failure, send a message from here.
	}
}